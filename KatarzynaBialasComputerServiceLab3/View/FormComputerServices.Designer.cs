﻿namespace KatarzynaBialasComputerServiceLab3
{
    partial class FormLab3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewComputerServices = new System.Windows.Forms.DataGridView();
            this.labelName = new System.Windows.Forms.Label();
            this.buttonShowAllServices = new System.Windows.Forms.Button();
            this.textBoxCost = new System.Windows.Forms.TextBox();
            this.labelCost = new System.Windows.Forms.Label();
            this.buttonCost = new System.Windows.Forms.Button();
            this.buttonShowComputers = new System.Windows.Forms.Button();
            this.buttonShowWorkers = new System.Windows.Forms.Button();
            this.buttonShowClients = new System.Windows.Forms.Button();
            this.buttonShowClientsWithService = new System.Windows.Forms.Button();
            this.buttonShowClientsComputer = new System.Windows.Forms.Button();
            this.labelClientName = new System.Windows.Forms.Label();
            this.textBoxClientName = new System.Windows.Forms.TextBox();
            this.textBoxClientSurname = new System.Windows.Forms.TextBox();
            this.labelClientSurname = new System.Windows.Forms.Label();
            this.labelShowComputerFromClient = new System.Windows.Forms.Label();
            this.buttonShowComputersFromClientName = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewComputerServices)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewComputerServices
            // 
            this.dataGridViewComputerServices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewComputerServices.Location = new System.Drawing.Point(12, 77);
            this.dataGridViewComputerServices.Name = "dataGridViewComputerServices";
            this.dataGridViewComputerServices.Size = new System.Drawing.Size(390, 360);
            this.dataGridViewComputerServices.TabIndex = 0;
            this.dataGridViewComputerServices.UseWaitCursor = true;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Arial Rounded MT Bold", 30F);
            this.labelName.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.labelName.Location = new System.Drawing.Point(6, 9);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(438, 46);
            this.labelName.TabIndex = 1;
            this.labelName.Text = "Serwis Komputerowy";
            this.labelName.UseWaitCursor = true;
            // 
            // buttonShowAllServices
            // 
            this.buttonShowAllServices.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.buttonShowAllServices.Location = new System.Drawing.Point(448, 345);
            this.buttonShowAllServices.Name = "buttonShowAllServices";
            this.buttonShowAllServices.Size = new System.Drawing.Size(191, 37);
            this.buttonShowAllServices.TabIndex = 2;
            this.buttonShowAllServices.Text = "Wyświetl Serwisy";
            this.buttonShowAllServices.UseVisualStyleBackColor = false;
            this.buttonShowAllServices.UseWaitCursor = true;
            this.buttonShowAllServices.Click += new System.EventHandler(this.buttonShowAllServices_Click);
            // 
            // textBoxCost
            // 
            this.textBoxCost.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxCost.Location = new System.Drawing.Point(448, 410);
            this.textBoxCost.Multiline = true;
            this.textBoxCost.Name = "textBoxCost";
            this.textBoxCost.Size = new System.Drawing.Size(120, 63);
            this.textBoxCost.TabIndex = 3;
            this.textBoxCost.UseWaitCursor = true;
            // 
            // labelCost
            // 
            this.labelCost.AutoSize = true;
            this.labelCost.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.labelCost.Location = new System.Drawing.Point(445, 394);
            this.labelCost.Name = "labelCost";
            this.labelCost.Size = new System.Drawing.Size(178, 13);
            this.labelCost.TabIndex = 4;
            this.labelCost.Text = "Wyświetl serwisy, gdzie koszt >= od:";
            this.labelCost.UseWaitCursor = true;
            // 
            // buttonCost
            // 
            this.buttonCost.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.buttonCost.Location = new System.Drawing.Point(574, 436);
            this.buttonCost.Name = "buttonCost";
            this.buttonCost.Size = new System.Drawing.Size(75, 23);
            this.buttonCost.TabIndex = 5;
            this.buttonCost.Text = "pokaż";
            this.buttonCost.UseVisualStyleBackColor = false;
            this.buttonCost.UseWaitCursor = true;
            this.buttonCost.Click += new System.EventHandler(this.buttonCost_Click);
            // 
            // buttonShowComputers
            // 
            this.buttonShowComputers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.buttonShowComputers.Location = new System.Drawing.Point(448, 34);
            this.buttonShowComputers.Name = "buttonShowComputers";
            this.buttonShowComputers.Size = new System.Drawing.Size(191, 37);
            this.buttonShowComputers.TabIndex = 6;
            this.buttonShowComputers.Text = "Wyświetl Komputery";
            this.buttonShowComputers.UseVisualStyleBackColor = false;
            this.buttonShowComputers.UseWaitCursor = true;
            this.buttonShowComputers.Click += new System.EventHandler(this.buttonShowComputers_Click);
            // 
            // buttonShowWorkers
            // 
            this.buttonShowWorkers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.buttonShowWorkers.Location = new System.Drawing.Point(448, 77);
            this.buttonShowWorkers.Name = "buttonShowWorkers";
            this.buttonShowWorkers.Size = new System.Drawing.Size(191, 37);
            this.buttonShowWorkers.TabIndex = 7;
            this.buttonShowWorkers.Text = "Wyświetl Pracowników";
            this.buttonShowWorkers.UseVisualStyleBackColor = false;
            this.buttonShowWorkers.UseWaitCursor = true;
            this.buttonShowWorkers.Click += new System.EventHandler(this.buttonShowWorkers_Click);
            // 
            // buttonShowClients
            // 
            this.buttonShowClients.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.buttonShowClients.Location = new System.Drawing.Point(448, 120);
            this.buttonShowClients.Name = "buttonShowClients";
            this.buttonShowClients.Size = new System.Drawing.Size(191, 37);
            this.buttonShowClients.TabIndex = 8;
            this.buttonShowClients.Text = "Wyświetl Klientów";
            this.buttonShowClients.UseVisualStyleBackColor = false;
            this.buttonShowClients.UseWaitCursor = true;
            this.buttonShowClients.Click += new System.EventHandler(this.buttonShowClients_Click);
            // 
            // buttonShowClientsWithService
            // 
            this.buttonShowClientsWithService.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.buttonShowClientsWithService.Location = new System.Drawing.Point(448, 300);
            this.buttonShowClientsWithService.Name = "buttonShowClientsWithService";
            this.buttonShowClientsWithService.Size = new System.Drawing.Size(191, 37);
            this.buttonShowClientsWithService.TabIndex = 9;
            this.buttonShowClientsWithService.Text = "Wyświetl klientów serwisowych";
            this.buttonShowClientsWithService.UseVisualStyleBackColor = false;
            this.buttonShowClientsWithService.UseWaitCursor = true;
            this.buttonShowClientsWithService.Click += new System.EventHandler(this.buttonShowClientsWithService_Click);
            // 
            // buttonShowClientsComputer
            // 
            this.buttonShowClientsComputer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.buttonShowClientsComputer.Location = new System.Drawing.Point(448, 163);
            this.buttonShowClientsComputer.Name = "buttonShowClientsComputer";
            this.buttonShowClientsComputer.Size = new System.Drawing.Size(191, 37);
            this.buttonShowClientsComputer.TabIndex = 10;
            this.buttonShowClientsComputer.Text = "Wyświetl Komputery Klientów";
            this.buttonShowClientsComputer.UseVisualStyleBackColor = false;
            this.buttonShowClientsComputer.UseWaitCursor = true;
            this.buttonShowClientsComputer.Click += new System.EventHandler(this.buttonShowClientsComputer_Click);
            // 
            // labelClientName
            // 
            this.labelClientName.AutoSize = true;
            this.labelClientName.Location = new System.Drawing.Point(454, 225);
            this.labelClientName.Name = "labelClientName";
            this.labelClientName.Size = new System.Drawing.Size(26, 13);
            this.labelClientName.TabIndex = 11;
            this.labelClientName.Text = "Imię";
            this.labelClientName.UseWaitCursor = true;
            // 
            // textBoxClientName
            // 
            this.textBoxClientName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxClientName.Location = new System.Drawing.Point(457, 241);
            this.textBoxClientName.Multiline = true;
            this.textBoxClientName.Name = "textBoxClientName";
            this.textBoxClientName.Size = new System.Drawing.Size(84, 20);
            this.textBoxClientName.TabIndex = 12;
            this.textBoxClientName.UseWaitCursor = true;
            // 
            // textBoxClientSurname
            // 
            this.textBoxClientSurname.AcceptsReturn = true;
            this.textBoxClientSurname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxClientSurname.Location = new System.Drawing.Point(550, 241);
            this.textBoxClientSurname.Multiline = true;
            this.textBoxClientSurname.Name = "textBoxClientSurname";
            this.textBoxClientSurname.Size = new System.Drawing.Size(84, 20);
            this.textBoxClientSurname.TabIndex = 13;
            this.textBoxClientSurname.UseWaitCursor = true;
            // 
            // labelClientSurname
            // 
            this.labelClientSurname.AutoSize = true;
            this.labelClientSurname.Location = new System.Drawing.Point(547, 225);
            this.labelClientSurname.Name = "labelClientSurname";
            this.labelClientSurname.Size = new System.Drawing.Size(53, 13);
            this.labelClientSurname.TabIndex = 14;
            this.labelClientSurname.Text = "Nazwisko";
            this.labelClientSurname.UseWaitCursor = true;
            // 
            // labelShowComputerFromClient
            // 
            this.labelShowComputerFromClient.AutoSize = true;
            this.labelShowComputerFromClient.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.labelShowComputerFromClient.Location = new System.Drawing.Point(448, 203);
            this.labelShowComputerFromClient.Name = "labelShowComputerFromClient";
            this.labelShowComputerFromClient.Size = new System.Drawing.Size(191, 13);
            this.labelShowComputerFromClient.TabIndex = 15;
            this.labelShowComputerFromClient.Text = "Komputery klienta o podanych danych:";
            this.labelShowComputerFromClient.UseWaitCursor = true;
            // 
            // buttonShowComputersFromClientName
            // 
            this.buttonShowComputersFromClientName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.buttonShowComputersFromClientName.Location = new System.Drawing.Point(448, 267);
            this.buttonShowComputersFromClientName.Name = "buttonShowComputersFromClientName";
            this.buttonShowComputersFromClientName.Size = new System.Drawing.Size(191, 22);
            this.buttonShowComputersFromClientName.TabIndex = 16;
            this.buttonShowComputersFromClientName.Text = "pokaż";
            this.buttonShowComputersFromClientName.UseVisualStyleBackColor = false;
            this.buttonShowComputersFromClientName.UseWaitCursor = true;
            this.buttonShowComputersFromClientName.Click += new System.EventHandler(this.buttonShowComputersFromClientName_Click);
            // 
            // FormLab3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::KatarzynaBialasComputerServiceLab3.Properties.Resources.binarybackgroundv4;
            this.ClientSize = new System.Drawing.Size(661, 512);
            this.Controls.Add(this.buttonShowComputersFromClientName);
            this.Controls.Add(this.labelShowComputerFromClient);
            this.Controls.Add(this.labelClientSurname);
            this.Controls.Add(this.textBoxClientSurname);
            this.Controls.Add(this.textBoxClientName);
            this.Controls.Add(this.labelClientName);
            this.Controls.Add(this.buttonShowClientsComputer);
            this.Controls.Add(this.buttonShowClientsWithService);
            this.Controls.Add(this.buttonShowClients);
            this.Controls.Add(this.buttonShowWorkers);
            this.Controls.Add(this.buttonShowComputers);
            this.Controls.Add(this.buttonCost);
            this.Controls.Add(this.labelCost);
            this.Controls.Add(this.textBoxCost);
            this.Controls.Add(this.buttonShowAllServices);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.dataGridViewComputerServices);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Name = "FormLab3";
            this.Text = "KatarzynaBialas Computer Service";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewComputerServices)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewComputerServices;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Button buttonShowAllServices;
        private System.Windows.Forms.TextBox textBoxCost;
        private System.Windows.Forms.Label labelCost;
        private System.Windows.Forms.Button buttonCost;
        private System.Windows.Forms.Button buttonShowComputers;
        private System.Windows.Forms.Button buttonShowWorkers;
        private System.Windows.Forms.Button buttonShowClients;
        private System.Windows.Forms.Button buttonShowClientsWithService;
        private System.Windows.Forms.Button buttonShowClientsComputer;
        private System.Windows.Forms.Label labelClientName;
        private System.Windows.Forms.TextBox textBoxClientName;
        private System.Windows.Forms.TextBox textBoxClientSurname;
        private System.Windows.Forms.Label labelClientSurname;
        private System.Windows.Forms.Label labelShowComputerFromClient;
        private System.Windows.Forms.Button buttonShowComputersFromClientName;
    }
}

