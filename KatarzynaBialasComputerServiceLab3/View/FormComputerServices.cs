﻿using KatarzynaBialasComputerServiceLab3.Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KatarzynaBialasComputerServiceLab3
{
    public partial class FormLab3 : System.Windows.Forms.Form
    {
        private SqlConnection sqlConnection;
        private SqlDataAdapter sqlDataAdapter;

        public FormLab3()
        {
            InitializeComponent();
            sqlConnection = new SqlConnection("Data Source = localhost; database = KatarzynaBialasComputerServiceLab3; Trusted_Connection= yes");
        }

        private void buttonShowAllServices_Click(object sender, EventArgs e)
        {
            Service.GetAllServices(sqlConnection, sqlDataAdapter, dataGridViewComputerServices);
        }

        private void buttonCost_Click(object sender, EventArgs e)
        {
            if (textBoxCost.Text != "")
                Service.GetService(sqlConnection, sqlDataAdapter, dataGridViewComputerServices, Int32.Parse(textBoxCost.Text));
            else
                MessageBox.Show("Brak kwoty");
        }

        private void buttonShowComputers_Click(object sender, EventArgs e)
        {
            Service.GetAllComputers(sqlConnection, sqlDataAdapter, dataGridViewComputerServices);
        }

        private void buttonShowWorkers_Click(object sender, EventArgs e)
        {
            Service.GetAllWorkers(sqlConnection, sqlDataAdapter, dataGridViewComputerServices);
        }

        private void buttonShowComputersFromClientName_Click(object sender, EventArgs e)
        {
            string clientName = textBoxClientName.Text;
            string clientSurname = textBoxClientSurname.Text;

            if(clientName != "" && clientSurname != "")
            {
                Service.GetAllComputersFromClientName(sqlConnection, sqlDataAdapter, dataGridViewComputerServices, clientName, clientSurname);
            }
            else
            {
                MessageBox.Show("Nie podano danych klienta!");
            }
        }

        private void buttonShowClients_Click(object sender, EventArgs e)
        {
            Service.GetAllClients(sqlConnection, sqlDataAdapter, dataGridViewComputerServices);
        }

        private void buttonShowClientsComputer_Click(object sender, EventArgs e)
        {
            Service.GetAllClientsComputers(sqlConnection, sqlDataAdapter, dataGridViewComputerServices);
        }

        private void buttonShowClientsWithService_Click(object sender, EventArgs e)
        {
            Service.GetClientsFromService(sqlConnection, sqlDataAdapter, dataGridViewComputerServices);
        }
    }
}
